import React from 'react';
import './InputStyle.css';

const Input = props => (
      <input placeholder={props.placeholder}
             onChange={props.changed}
             value={props.value}
             type='text'
             className="input"
      />
    );

export default Input;
