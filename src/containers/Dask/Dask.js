import React, { Component } from 'react';
import './Dask.css';


class Dask extends Component {

state = {
  statusNewMessage : false,
}

notice = () => this.state.statusNewMessage? 'show' : 'hide';

hideNotice() {
  setTimeout(() => {
    this.setState({statusNewMessage: false})
  }, 1000);
};

  componentWillReceiveProps (nextProps){
    const oldLastMessage = this.props.messages[this.props.messages.length -1];
    const newLastMessage = nextProps.messages[nextProps.messages.length -1];

      if(
        ((newLastMessage !== undefined? newLastMessage.author: null) !== this.props.author) && ((oldLastMessage !== undefined? oldLastMessage.datetime: null) !== newLastMessage.datetime)){
        this.setState({statusNewMessage: true});
      };
      this.hideNotice();
  };

  render(){
  return (
    <div id='containerMessage' className="container">
      {this.props.messages.map((message, index) =>{
        return <div className="message" key={index}>
          <p className="time">{message.datetime.slice(11, 20)}</p>
          <p className="author">{message.author}</p>
          <p className="said">{message.message}</p>
        </div>
      })}
      <p className={this.notice()}>Новое сообщение</p>
    </div>
  )
}
};

export default Dask;
