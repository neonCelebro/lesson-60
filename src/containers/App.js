import React, { Component } from 'react';
import './App.css';
import Dask from './Dask/Dask';
import Button from '../components/UI/Button/Button';
import Input from '../components/UI/Input/Input';

class App extends Component {
  state = {
    messages: [],
    author: 'Kairat & Oleg',
    message: '',
  };
  changeStatusNewMessages = () =>{
  };
  getAuthor = (e) => this.setState({author: e.target.value});
  getMessage = (e) => this.setState({message: e.target.value});

  sendMessage = () =>{
    const URL = 'http://146.185.154.90:8000/messages';
    const data = new URLSearchParams();

    data.append('message', this.state.message);
    data.append('author', this.state.author);

    const config = {
      method: 'POST',
      body: data,
    };
    fetch(URL, config);
    this.setState({message : ''});
    };

    getMessages() {
        const url = 'http://146.185.154.90:8000/messages';
        fetch(url).then((response) => {
                return response.json();
           }).then((response) => {
             this.setState({messages : response});
           });
    };

    intervalFromMessages() {
      this.intervalId = setInterval(() => {
        this.getMessages();
      }, 2000);
    };

    lastMessages(){
      const block = document.getElementById("containerMessage");
      if (block.scrollTop === 0) {
        block.scrollTop = block.scrollHeight;
      }
    };
    componentDidMount() {
      this.intervalFromMessages();
    };
    componentDidUpdate(){
          this.lastMessages();
    };
    componentWillUnmount(){
      clearInterval(this.intervalId);
    }
  render() {
    return (
      <div className="App">
        <Dask
          author={this.state.author}
          messages={this.state.messages}/>
        <Input
          changed={this.getAuthor}
          value={this.state.author}
        />
        <Input
          placeholder='message'
          changed={this.getMessage}
          value={this.state.message}
        />
        <Button
          btnType='Purple'
          label="Send"
          cliked={this.sendMessage}
        />
      </div>
    );
  }
};

export default App;
